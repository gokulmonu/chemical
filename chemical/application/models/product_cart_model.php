<?php

class Product_cart_model extends MY_Model
{
    function __construct()
    {
        //$this->CI = & get_instance();
        parent::__construct();
        $this->load->library('Base', 'base');

    }

    protected $tableName = "sb_product_cart";

    public $formFields = [
        'id' => 'integer',
        'product_id' => 'integer',
        'user_id' => 'integer',
        'p_quantity' => 'integer',
        'is_checkout' => 'integer',
    ];

    public function add($dataArr)

    {
        $resultArr = $this->base->add($dataArr, $this->formFields);
        if ($resultArr) {
            $this->db->insert($this->tableName, $resultArr);
            return $this->db->insert_id();
        } else {
            return false;
        }

    }

    public function update($dataArr, $whereArr)
    {
        $whereArrSanitize = $this->base->validate($whereArr, $this->formFields);
        $resultArr = $this->base->update($dataArr, $this->formFields);
        if (count($resultArr) && count($whereArrSanitize)) {
            $this->db->update($this->tableName, $resultArr, $whereArrSanitize);
            return $this->db->insert_id();
        } else {
            return false;
        }
    }

    public function delete($whereArr, $strongDelete = false)
    {
        $whereArrSanitize = $this->base->validate($whereArr, $this->formFields);
        if (isset($whereArrSanitize) && !empty($whereArrSanitize)) {
            if ($strongDelete) {
                $this->db->delete($this->tableName, $whereArrSanitize);
            } else {
                $resultArr = $this->base->delete();
                $this->db->update($this->tableName, $resultArr, $whereArrSanitize);
            }
        }
    }

    public function getCart($userId)
    {
        $sql = "SELECT p.*,c.id as cart_id,c.p_quantity FROM `sb_product_cart` c LEFT JOIN sb_product p ON p.id= c.product_id WHERE c.user_id = '$userId' and c.is_checkout = 0 AND c.is_deleted is null AND p.is_deleted is null";
        $query = $this->db->query($sql);
        $resultArr = $query->result_array();
        return $resultArr;
    }

}