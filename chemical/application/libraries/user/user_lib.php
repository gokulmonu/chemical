<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_lib
{

    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->model('user/user_model','user_model');
    }

    public function addUser($dataArr)
    {
        $userId = $this->CI->user_model->add($dataArr);
        $this->CI->load->model('user/user_group_model','user_group_model');
        if(isset($dataArr['is_admin']) && $dataArr['is_admin'] ){
            $tempArr = [
                'user_id' => $userId,
                'group_id' => 1
            ];
            $this->CI->user_group_model->add($tempArr);
        }else{
            $tempArr = [
                'user_id' => $userId,
                'group_id' => 2
            ];
            $this->CI->user_group_model->add($tempArr);
        }
        return $userId;
    }
}