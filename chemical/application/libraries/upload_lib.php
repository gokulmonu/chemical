<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload_lib
{

    public $fields = [];

    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->model('user/user_model','user_model');
    }

    public function doUpload()
    {
        $data =[];
        $config['upload_path'] = 'uploads/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']      = 2014;
        $config['max_width']     = 1980;
        $config['max_height']    = 1980;
        $this->CI->load->library('upload', $config);

        if ($this->CI->upload->do_upload('file')) {
            $data = array('upload_data' => $this->CI->upload->data());
            if(isset($data['upload_data']['full_path']) && $data['upload_data']['full_path']){
              $this->resizeImage($data['upload_data']['full_path']);
            }
            return $data;
        }else{
            $error = array('error' => $this->CI->upload->display_errors());
            return $error;
        }

    }

    public function resizeImage($fileURl)
    {
        $target_path = 'uploads/thumbnail/';
        $config_manip = array(
            'image_library' => 'gd2',
            'source_image' => $fileURl,
            'new_image' => $target_path,
            'maintain_ratio' => TRUE,
            'create_thumb' => TRUE,
            'thumb_marker' => '_thumb',
            'width' => 150,
            'height' => 150
        );
        $this->CI->load->library('image_lib', $config_manip);
        $this->CI->image_lib->resize();
        $this->CI->image_lib->clear();

    }

}