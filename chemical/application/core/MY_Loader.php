<?php
class MY_Loader extends CI_Loader {
    public function template($template_name, $vars = array(), $return = FALSE)
    {
        $content  = $this->view('header');
        $content .= $this->view($template_name, $vars, $return);
        $content .= $this->view('footer');

        if ($return)
        {
            return $content;
        }
    }
}