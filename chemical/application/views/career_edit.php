
<script>

    $(function () {

        $("#career_form").submit(function( event ) {
            event.preventDefault();
            var formData = getFormData();
            $.ajax({
                url: '<?php echo base_url()?>career/validate',
                type: 'POST',
                data:  formData,
                success: function (data) {
                    $('#product_name_error').html('');
                    if(data.success){
                        window.location.href ='<?php echo base_url() ?>careers';
                    }

                    if(!data.success){
                        for(key in data){
                            $('#'+data[key]['field_id']).html(data[key]['label']);
                        }
                    }

                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });

        });

        function getFormData() {

            var data = {};
            data['id'] = $('#id').val();
            data['title'] = $('#title').val();
            data['salary'] = $('#salary').val();
            data['experiance'] = $('#experiance').val();
            data['description'] = $('#description').val();
            return data;
        }

    });
</script>


<section class="s-wrp s-hi-pad"><!-- section wrp-->
    <div class="s-container"><!-- s-container-->

        <div class="s-wrp"><!--s-wrp-->

            <div class="s-row"><!--s row-->

                <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12"><!--s col-->

                    <article class="s-wrp edit-block">

                        <h2 class="main-title">Add / Edit Career</h2>

                    </article>
                </div>
                <!--/. s col-->

            </div>
            <!--/. s row-->

        </div>
        <!--/. s-wrp-->
    </div>
    <!--/. s-container-->
</section>
<!--/. section wrp-->


<section class="s-wrp"><!-- section wrp-->
    <div class="s-container"><!-- s-container-->

        <div class="s-wrp"><!--s-wrp-->

            <div class="s-row"><!--s row-->

                <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12"><!--s col-->


                    <form id="career_form" class="custom-form">
                        <input type="hidden" id="id" value="<?php if(isset($id))echo $id; ?>">
                        <div class="s-form-group">
                            <label for="title">Title:</label>
                            <input type="text" id="title" name="title" placeholder="Enter title" value="<?php if(isset($title))echo $title; ?>">
                            <span class="error-msg" id="title_error"></span>
                        </div>

                        <div class="s-form-group">
                            <label for="description">Description:</label>
                            <textarea id="description"  placeholder="Enter  Description" name="description"><?php if(isset($description))echo $description; ?></textarea>
                            <span class="error-msg" id="description_error"></span>
                        </div>

                        <div class="s-form-group">
                            <label for="salary">Salary:</label>
                            <input type="text" id="salary" name="salary" placeholder="Enter Salary" value="<?php if(isset($salary))echo $salary; ?>">
                            <span class="error-msg" id="salary_error"></span>
                        </div>

                        <div class="s-form-group">
                            <label for="experiance">Experiance:</label>
                            <input type="text" id="experiance" name="experiance" placeholder="Enter experiance" value="<?php if(isset($experiance))echo $experiance; ?>">
                            <span class="error-msg" id="experiance_error"></span>
                        </div>

                        <div class="s-form-group s-wrp">
                            <input type="submit" class="pg-btn" value="Save">
                        </div>
                    </form>


                </div>
                <!--/. s col-->

            </div>
            <!--/. s row-->

        </div>
        <!--/. s-wrp-->
    </div>
    <!--/. s-container-->
</section>
<!--/. section wrp-->

