<section class="s-wrp s-md-pad brdr-tp"><!--second_section-->
    <div class="s-container"><!--container-->


        <div class=" s-row"><!--s-row-->
            <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12"><!-- s-col-->


                <article class="s-wrp about-box">


                    <article class="s-wrp solution-extra-area">
                        <br><br>
<!--                        <span class="abt-img animated wow fadeInLeft" data-wow-delay="15ms"><img src="<?php /*echo base_url() */?>bootstrap/images/logo.png"></span>
-->
                        <?php if(isset($data[0]['introduction'])){?>
                            <h4>Introduction</h4>
                            <?php
                            echo nl2br($data[0]['introduction']);
                        }?>

                    </article>

                    <article class="s-wrp solution-extra-area">


                        <?php if(isset($data[0]['introduction'])){?>
                            <h4 class="about-title">Mission Statement</h4>
                            <?php
                            echo nl2br($data[0]['mission']);
                        }?>
                    </article>

                    <article class="s-wrp solution-extra-area">


                        <?php if(isset($data[0]['introduction'])){?>
                            <h4 class="about-title">Objectives</h4>
                            <?php
                            echo nl2br($data[0]['objectives']);
                        }?>

                    </article>
                    <article class="s-wrp solution-extra-area">



                        <?php if(isset($data[0]['introduction'])){?>
                            <h4 class="about-title">Our Principles and Values</h4>
                            <?php
                            echo nl2br($data[0]['principles']);
                        }?>

                    </article>

                </article>

            </div><!--/. s-col-->

        </div><!--/. s-row-->


    </div><!--/. container-->
</section><!--/. second_section-->

<section class="s-wrp s-hi-pad clients-wrp"><!--clients-wrp-->

    <div class="s-container"><!-- s-container-->

        <h2 class="sub-title">Our Members</h2>


        <article class="client-block s-wrp"><!--client-block-->

            <div class="owl-carousel clients-slide"><!--clients-slide-->

                <?php if(isset($members)){

                    foreach($members as $value){
                        $thumbPath = '';
                        if(isset($value['image'])){
                            $path = base_url();
                            $name = substr($value['image']['file_name'],0,strrpos($value['image']['file_name'],'.'));
                            $ext = substr($value['image']['file_name'],strrpos($value['image']['file_name'],'.'));
                            $thumbPath = $path.'uploads/thumbnail/'.$name.'_thumb'.$ext;
                        }

                        ?>
                        <div class="item"><!--item-->

                            <span class="client-logo"><img src="<?php echo $thumbPath?>" alt="client"></span>
                            <h6>Name :<?php echo $value['name']?></h6>
                            <h6>Position :<?php echo $value['position']?></h6>
                            <h6>Email :<?php echo $value['email']?></h6>
                            <h6>Phone :<?php echo $value['phone']?></h6>

                        </div><!--/. item-->



                        <?php
                    }
                }


                ?>

            </div><!--/. clients-slide-->


        </article><!--/. client-block-->



    </div><!--/. s-container-->

</section><!--/. clients-wrp-->