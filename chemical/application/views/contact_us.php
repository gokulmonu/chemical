<script>
    $(function () {
        $("#contact_form").submit(function( event ) {
            event.preventDefault();
            var name = $('#name').val();
            var email = $('#email').val();
            var phone = $('#phone').val();
            var message = $('#message').val();
            $.ajax({
                url: '<?php echo base_url()?>contactus/send-email',
                type: 'POST',
                data: {'name' :name,'email':email,'phone':phone,'message':message},
                success: function (data) {
                    if(data.success){
                        alert("We have received your submission. Our admin will contact you as soon as possible");
                        window.location.href ='<?php echo base_url() ?>';
                    }

                },
                error: function (e) {
                    //called when there is an error
                    //console.log(e.message);
                }
            });

        });
    });



    function initMap() {
        var uluru = {lat: -25.363, lng: 131.044};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 4,
            center: uluru
        });
        var marker = new google.maps.Marker({
            position: uluru,
            map: map
        });
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCxL6dcqSf_0YMSlpXcM5Pa5YGs05xDyOs&callback=initMap">
</script>

<section class="s-wrp s-hi-pad "><!--fourth_section-->
    <div class="s-container"><!--container-->
        <article class="s-wrp s-hi-pad partner-box contact-box"><!--s-wrp-->

            <div class="s-wrp contact-box ">
                <div class=" s-row"><!--s-row-->
                    <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12"><!-- s-col-->

                        <div id="map">


                        </div>

                    </div><!--/. s-col-->

                    <div class="s-col-lg-6 s-col-md-6 s-col-sm-12 s-col-xs-12"><!-- s-col-->
                        <article class="s-wrp adress-block">
                            <div class="address-item s-wrp">
                                <div class=" s-row"><!--s-row-->
                                    <div class="s-col-lg-12 s-col-md-12 s-col-sm-12 s-col-xs-12 "><!-- s-col-->
                                        <span class="s-wrp s-md-pad">    <h4 class=" nira-blue">OUR ADDRESS</h4>


                                     <span class="divide-address s-wrp">
                                            <p>Glat Co. Pvt. Ltd</p>
                                           <p> Ma Uthuruvehi</p>
                                            <p>Chaandhanee Magu</p>
                                            <p>Republic of maldives</p>
                                         </span>



                                          </span>
                                        <span class="s-wrp s-md-pad">
                                         <p>Phone:<a class="tel-num">3329378</a></p>
                                         <p>E-mail:<a class="tel-num">info@glatcomaldives.com</a></p>
                                       </span>

                                </div><!--/. s-row-->
                            </div>



                        </article>

                    </div><!--/. s-col-->

                    <div class="s-col-lg-6 s-col-md-6 s-col-sm-12 s-col-xs-12"><!-- s-col-->
                        <article class="s-wrp adress-block">
                            <div class="s-wrp form-box">
                                <form class="contact-form custom-form" id="contact_form">
                                    <h4 class=" nira-blue">Contact Form</h4>
                                    <p id="returnmessage"></p>
                                    <div class="s-form-group">
                                    <input type="text" id="name"  placeholder="Name">
                                    </div>
                                    <div class="s-form-group">
                                    <input type="text" placeholder="E-mail" id="email" class="email">
                                    </div>
                                    <div class="s-form-group">
                                    <input type="text" id="phone" placeholder="Phone" class="phone">
                                    </div>
                                    <div class="s-form-group">
                                    <textarea class="message-txt" id="message" placeholder="Message"></textarea>
                                    </div>
                                    <div class="s-form-group">
                                    <button type="submit"  id="submit" value="Send Message" class="pg-btn" >SEND</button>
                                    </div>

                                </form>


                            </div>


                        </article>

                    </div><!--/. s-col-->
                </div><!--/. s-row-->
            </div>



        </article><!--/. s-wrp-->
    </div><!--/. container-->
</section><!--/. fourth_section-->





