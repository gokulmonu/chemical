
<footer class="s-wrp">  <!--footer-->

    <section class="footer-wrp  s-hi-pad s-wrp"><!--footer-wrp -->

        <div class="s-container"><!-- s-container-->

            <div class="s-row"><!--f row-->

                <div class="s-col-lg-4 s-col-md-4 s-col-sm-12 s-col-xs-12"><!--f col-->

                    <article class="s-wrp each-footer-block"><!--each-footer-block-->

                        <span class="footer-logo s-sec-block"><img src="<?php echo base_url() ?>bootstrap/images/logo.png" alt="footer logo"></span>

                        <div class="footer-content s-wrp"><!--footer-content-->

                            <p>Glat Co. Pvt Ltd founded in 2016, is an established supply chain organization. GlatCo. Is a Maldives based company, owned by it's managers and managed by it's owners.</p>

                            <div class="quick-contact-footer"><!--quick-contact-footer-->

                                <ul>
                                    <li><span class="foot-addr">Glat Co. Pvt. Ltd
                                            <br>
                                            Ma Uthuruvehi
                                            <br>
                                            Chaandhanee Magu
                                            <br>
                                            Republic of maldives</span></li>
                                    <li><span class="foot-fax">3329378</span></li>
                                    <li><span class="foot-mail">info@glatcomaldives.com</span></li>
                                </ul>


                            </div><!--/. quick-contact-footer-->


                        </div><!--/. footer-content-->


                    </article><!--/. each-footer-block-->

                </div><!--/. f col-->


                <div class="s-col-lg-4 s-col-md-4 s-col-sm-12 s-col-xs-12"><!--f col-->

                    <article class="s-wrp each-footer-block"><!--each-footer-block-->

                        <h3 class="footer-title">navigation</h3>

                        <div class="footer-content s-wrp"><!--footer-content-->

                            <div class="quick-nav-footer"><!--quick-nav-footer-->

                                <ul>

                                    <li><a href="#">home</a></li>
                                    <li><a href="#">About us</a></li>
                                    <li><a href="#">service</a></li>
                                    <li><a href="#">features</a></li>
                                    <li><a href="#">Projects</a></li>
                                    <li><a href="#">news</a></li>
                                    <li><a href="#">Contact us</a></li>
                                    <li><a href="#">get brochure</a></li>

                                </ul>



                            </div><!--/. quick-nav-footer-->


                        </div><!--/. footer-content-->


                    </article><!--/. each-footer-block-->



                    <article class="s-wrp s-md-pad each-footer-block"><!--each-footer-block-->

                        <h3 class="footer-title">socialize with us</h3>

                        <p class="cnt-txt">Contact us via social networks</p>

                        <div class="footer-content social-footer-wrp s-wrp"><!--footer-content-->

                            <div class="social-nav-footer"><!--social-nav-footer-->

                                <ul>

                                    <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                                    <li><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>

                                </ul>



                            </div><!--/. social-nav-footer-->


                        </div><!--/. footer-content-->


                    </article><!--/. each-footer-block-->


                </div><!--/. f col-->



                <div class="s-col-lg-4 s-col-md-4 s-col-sm-12 s-col-xs-12"><!--f col-->

                    <article class="s-wrp each-footer-block"><!--each-footer-block-->

                        <h3 class="footer-title">Working Hours</h3>

                        <div class="footer-content s-wrp"><!--footer-content-->

                            <span>Visit us at our HQ for a mean cup of coffe and a fantastic consulting team.</span>


                            <div class="time-table-footer"><!--time-table-footer-->

                                <table class="working-hours">
                                    <tbody>
                                    <tr>
                                        <th>Monday</th>
                                        <td>9am > 6pm</td>
                                    </tr>
                                    <tr>
                                        <th>Tuesday</th>
                                        <td>9am > 6pm</td>
                                    </tr>
                                    <tr>
                                        <th>Wendsday</th>
                                        <td>9am > 6pm</td>
                                    </tr>
                                    <tr>
                                        <th>Thursday</th>
                                        <td>9am > 6pm</td>
                                    </tr>
                                    <tr>
                                        <th class="important">Friday</th>
                                        <td class="important">Closed</td>
                                    </tr>

                                    <tr>
                                        <th>Saturday</th>
                                        <td>9am > 6pm</td>
                                    </tr>
                                    <tr>
                                        <th>Sunday</th>
                                        <td>9am > 6pm</td>
                                    </tr>

                                    </tbody>

                                </table>


                            </div><!--/. time-table-footer-->


                        </div><!--/. footer-content-->


                    </article><!--/. each-footer-block-->


                </div><!--/. f col-->



            </div><!--/. f row-->


        </div><!--/. s-container-->


    </section><!--/. footer-wrp -->


    <section class="copyright-wrp s-wrp"><!--copyright-wrp-->

        <div class="s-container"><!-- s-container-->

            <p class="copy-txt">Copyright &copy; 2016 Chemical Industrial </p>

        </div><!--/. s-container-->

    </section><!--/. copyright-wrp-->


</footer><!--/. footer-->


</div><!--/. s-pgwrp-->



</body>
</html>