<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user/user_model','user_model');
        $this->load->library('core_lib');
        $this->load->library('form');
        $this->load->library('user/user_lib','user_lib');
        $this->load->model('product_model','product_model');
    }

    public function _remap($method)
    {
        $userGroup = $this->core_lib->getUserGroup();
        if($method == "add"){
               $this->add();
        }elseif ($method == "validate"){
                $this->validate();
        }
    }
    public function add()
    {
        $this->load->view('signup');
    }
    public function validate()
    {

        $password = $this->input->post('password');
        $username = $this->input->post('username');
        $name = $this->input->post('name');
        $mobile = $this->input->post('mobile');
        $email = $this->input->post('email');
        $decodedString = base64_decode($password);
        $this->form->setRules($username,'username_error','Please enter your username','',3,20);
        $this->form->setRules($decodedString,'password_error','Please enter your password','',3);
        $this->form->setRules($name,'name_error','Please enter your name','',3,20);
        $this->form->setRules($mobile,'mobile_error','Please enter valid mobile number','integer');
        $this->form->setRules($email,'email_error','Please enter your email','email');
        $validationArr = $this->form->run();
        $this->load->model('user/user_model','user_model');
        $isUsernameExist = $this->user_model->isValueExist('username',$username);
        if($isUsernameExist){
            if($validationArr){
                $key = array_search('username_error', array_column($validationArr, 'field_id'));
                if($key !== false){
                    $validationArr[$key]['label'] = "Username already exist";
                }
            }else{
                $tempArr = [
                    'field_id' =>'username_error',
                    'label' => 'Username already exist'
                ];
                $validationArr[] = $tempArr;
            }
        }

        if(is_array($validationArr) && count($validationArr) && $validationArr != false){
            header('Content-Type: application/json');
            echo json_encode( $validationArr );
        }else{
            $passwordHash = password_hash($decodedString, PASSWORD_DEFAULT);
            $dataArr = [
                'first_name' => $name,
                'email' => $email,
                'phone' => $mobile,
                'password' => $passwordHash,
                'forgot_token' => NULL,
                'username' => $username,
                'ip_address' => 'string',
                'is_active' => 1,
                'is_blocked' => 0,
                'is_admin' =>false

            ];
            $this->user_lib->addUser($dataArr);
            $redirectUrl['url'] = [
                'success' => true
            ];
            header('Content-Type: application/json');
            echo json_encode( $redirectUrl );
        }


    }




}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */