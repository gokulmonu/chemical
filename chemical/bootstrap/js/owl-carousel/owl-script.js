$(document).ready(function(){
	
	
	$(".recent-prjct-slide").owlCarousel({
        autoPlay: 3000,
        items : 3,
		autoPlay : false,
		navigation : true,
		pagination : false,
		navigationText : ["",""],
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,2],
		itemsTablet: [768,2],
    	itemsMobile : [479,1],
      });
	
	
	$(".testi-slide").owlCarousel({
        autoPlay: 3000,
        items : 2,
		autoPlay : false,
		navigation : true,
		pagination : false,
		navigationText : ["",""],
        itemsDesktop : [1199,2],
        itemsDesktopSmall : [979,2],
		itemsTablet: [768,1],
    	itemsMobile : [479,1],
      });
	
	
	
	
	
	$(".clients-slide").owlCarousel({
        autoPlay: 3000,
        items : 5,
		autoPlay : true,
		navigation : true,
		pagination : false,
		navigationText : ["",""],
        itemsDesktop : [1199,5],
        itemsDesktopSmall : [979,4],
		itemsTablet: [768,3],
    	itemsMobile : [479,3],
      });


	
	
						   
});